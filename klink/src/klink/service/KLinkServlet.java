package klink.service;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Servlet implementation class ContactServlet
 */
public class KLinkServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private WebApplicationContext ctx = null;
      
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KLinkServlet() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		ctx = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/javascript");
		String method = request.getParameter("method");
		String callback = request.getParameter("callback");
		KLinkDao klinkDao = (KLinkDao)ctx.getBean("klinkDao");
		if(method != null && method.trim().length()>0 ){
			if(method.equalsIgnoreCase("getContactCount")){
				//获得联系人数量
				this.getContactCount(klinkDao,request,response,callback);
			}else if(method.equalsIgnoreCase("getInfoByPy")){
				//根据参数查询结果
				this.getInfoByPy(klinkDao,request,response,callback);
			}else if(method.equalsIgnoreCase("sendMail")){
				//发送邮件
				this.sendEMail(request,response,callback);
			}else if(method.equalsIgnoreCase("getAllContact")){
				//发送邮件
				this.getAllContact(klinkDao,request,response,callback);
			}else if(method.equalsIgnoreCase("saveContact")){
				//保存联系人信息
				this.saveContact(klinkDao,request,response,callback);
			}
		}else{
			response.getWriter().write("The request was not allowed");
		}
	}

	private void saveContact(KLinkDao klinkDao, HttpServletRequest request,
			HttpServletResponse response, String callback) throws IOException {
		String id = new String(request.getParameter("id").getBytes("ISO-8859-1"),"UTF-8");
		String cname = new String(request.getParameter("cname").getBytes("ISO-8859-1"),"UTF-8");
		String cgroup = new String(request.getParameter("cgroup").getBytes("ISO-8859-1"),"UTF-8");
		String cphone = new String(request.getParameter("cphone").getBytes("ISO-8859-1"),"UTF-8");
		String cmail = new String(request.getParameter("cmail").getBytes("ISO-8859-1"),"UTF-8");
		
		String output = callback+"("+klinkDao.saveContact(id, cname, cgroup, cphone, cmail)+")";
		response.getWriter().write(output);
	}

	private void getAllContact(KLinkDao klinkDao, HttpServletRequest request,
			HttpServletResponse response, String callback) throws IOException {
		String output = callback+"("+klinkDao.getAllContact()+")";
		response.getWriter().write(output);
	}

	private void sendEMail(HttpServletRequest request,
			HttpServletResponse response,String callback) throws IOException {
		String emailAddr = new String(request.getParameter("to").getBytes("ISO-8859-1"),"UTF-8");
		String msg = new String(request.getParameter("msg").getBytes("ISO-8859-1"),"UTF-8");
		String title = new String(request.getParameter("title").getBytes("ISO-8859-1"),"UTF-8");
		
		StringBuilder sb4Msg = new StringBuilder();
		sb4Msg.append("来自").append("osc_mopaas_klink").append("的邮件：\n\n").append(msg);
		
		boolean returnSts = sendEmail(title,emailAddr,sb4Msg.toString());

		String output = callback+"({})";
		if(returnSts){
			output = callback+"({\"result\" : \"1\"})";//发送成功
		}else{
			output = callback+"({\"result\" : \"0\"})";//发送失败
		}
		response.getWriter().write(output);
	}

	private void getInfoByPy(KLinkDao klinkDao, HttpServletRequest request,
			HttpServletResponse response,String callback) throws IOException {
		String py = request.getParameter("py");
		String output = callback+"("+klinkDao.getInfoByPy(py)+")";
		response.getWriter().write(output);
	}

	private void getContactCount(KLinkDao klinkDao,
			HttpServletRequest request, HttpServletResponse response,String callback) throws IOException {
		String output = callback+"("+klinkDao.getContactCount()+")";
		response.getWriter().write(output);
	}
	
	/**
	 * 发送电子邮件
	 * @param title
	 * @param emailAddr
	 * @param msg
	 * @throws EmailException
	 */
	private boolean sendEmail(String title,String emailAddr,String msg){
		try {
			Email email = new SimpleEmail();
		
			email.setHostName("smtp.163.com");
			email.setAuthentication("osc_mopaas_klink", "密码");
			email.setFrom("osc_mopaas_klink@163.com");
			
			email.setSubject(title);
			email.setCharset("utf8");
			email.setMsg(msg);
			email.addTo(emailAddr);
			email.send();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
